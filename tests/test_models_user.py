from app.models import User
from sqlalchemy.exc import IntegrityError

def test_model(session):
    try:
        user = User(name='testuser',
                    email='test@user.com',
                    google_id='123456')
    except:
        assert False

    session.add(user)
    session.commit()

    assert user.id is not None
    assert user.member_since is not None

    token = user.generate_auth_token(3600)
    user_verified = User.verify_auth_token(token)
    assert user.id == user_verified.id

def is_user_valid(session, name, email, google_id):
    try:
        user = User(name=name,
                    email=email,
                    google_id=google_id)

        session.add(user)
        session.commit()
        return True
    except (AssertionError, IntegrityError) as e:
        return False

    assert False
    
def test_validation(session):
    # name must be more than 2 characters
    assert not is_user_valid(session, 'a', 'test@user.com', '123456')

    # email address must be valid
    assert not is_user_valid(session, 'testuser', 'abc', '123456')

    assert is_user_valid(session, 'testuser', 'test@user.com', '123456')
    # duplicate
    assert not is_user_valid(session, 'testuser', 'test@user.com', '123456')

