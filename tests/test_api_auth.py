import os

from flask import g

ID_TOKEN = os.environ.get('ID_TOKEN') 
AUTH_API = 'api/v1.0/auth/'

def login(client, id_token=None):
  json = {}
  if id_token is not None:
    json['id_token'] = id_token

  return client.post(AUTH_API + 'signin/', json=json)

def register(client, id_token=None, name=None):
  json = {}
  if id_token is not None:
    json['id_token'] = id_token
  if name is not None:
    json['name'] = name

  return client.post(AUTH_API + 'register/', json=json)

def test_signin(client):
  # No id_token
  rv = login(client)
  assert rv.status_code == 401

  # Invalid id_token
  rv = login(client, 'abc')
  assert rv.status_code == 401

  # Valid id_token, but unregistered
  rv = login(client, ID_TOKEN)
  assert rv.status_code == 200
  assert rv.get_json()['registered'] == False

def test_register(client):
  # No id_token
  rv = register(client)
  assert rv.status_code == 401

  # Missing name parameter
  rv = register(client, ID_TOKEN)
  assert rv.status_code == 400

  # name is shorter than 3 characters
  rv = register(client, ID_TOKEN, 'a')
  assert rv.status_code == 400

  # Sign up
  rv = register(client, ID_TOKEN, 'test_api_auth')
  assert rv.status_code == 200

  # Username is used
  rv = register(client, ID_TOKEN, 'test_api_auth')
  assert rv.status_code == 400

def test_verify_token(client):
  # Login after register
  rv = login(client, ID_TOKEN)
  assert rv.status_code == 200
  assert rv.get_json()['registered'] == True

  # Valid token
  token = rv.get_json()['token']
  with client as c:
    c.post(AUTH_API + 'signin/', 
           json={'id_token': ID_TOKEN},
           headers={'Authorization': 'JWT ' + token})
    assert g.user is not None
    assert g.authenticated

  # Invalid token
  with client as c:
    c.post(AUTH_API + 'signin/', 
           json={'id_token': ID_TOKEN},
           headers={'Authorization': 'JWT ' + 'this is not a token'})
    assert g.user is None
    assert not g.authenticated

