import os
import pytest

from flask import appcontext_pushed, g

from app import create_app, db
from app.models import User

@pytest.fixture(scope='session')
def app():
    app = create_app('TEST')

    with open(os.path.join(os.path.dirname(__file__), './data.sql'), 'rb') as f:
        data_sql = f.read().decode('utf8')

    with app.app_context():
        db.engine.execute(data_sql)

    yield app

    with app.app_context():
        db.drop_all()
        db.create_all()

@pytest.fixture(scope='function')
def session(app, request):
    ctx = app.app_context()
    ctx.push()

    connection = db.engine.connect()
    transaction = connection.begin()

    options = dict(bind=connection, binds={})
    session = db.create_scoped_session(options=options)

    db.session = session

    def teardown():
        transaction.rollback()
        session.close()
        ctx.pop()

    request.addfinalizer(teardown)
    return session

@pytest.fixture(scope='function')
def client(app):
    yield app.test_client()

@pytest.fixture(scope='function')
def client_loggedin(app):
    with app.app_context():
        user = User.query.first()

    def handler(sender, **kwargs):
        g.user= user
        g.authenticated = True

    with appcontext_pushed.connected_to(handler, app):
        yield app.test_client()

