import io
import os

from flask import g

USERS_API = 'api/v1.0/me/'
ANDROID_HEADERS = {
    'User-Agent': 'mydashcam-android',
    'x-android-model': 'android-model',
    'x-android-serial': 'android-serial'
}

def upload_url(c, path, headers=ANDROID_HEADERS):
    return c.get(
               USERS_API + "upload_url/?path=" + path,
               headers=headers
           )

def test_userinfo(client_loggedin):
    with client_loggedin as c:
        rv = c.get(USERS_API)
        assert g.authenticated

    assert rv.status_code == 200

    user_json = rv.get_json()
    assert 'name' in user_json
    assert 'email' in user_json

def test_upload_url(client_loggedin, session):
    # Retrieve same upload url when route is same
    path = "/abc/abc/2018-06-21--14-51-35--0/acamera"

    rv = upload_url(client_loggedin, path)
    assert rv.status_code == 200
    url_json1 = rv.get_json()
  
    rv = upload_url(client_loggedin, path)
    assert rv.status_code == 200
    url_json2 = rv.get_json()

    assert url_json1['url'] == url_json2['url']

    # Retrieve upload url for duplicate route name
    # but different device model and serial
    path = "/abc/abc/2018-06-20--12-51-35--0/acamera"
    headers_1 = {
        'User-Agent': 'mydashcam-android',
        'x-android-model': 'model_1',
        'x-android-serial': '123456'
    }
    headers_2 = {
        'User-Agent': 'mydashcam-android',
        'x-android-model': 'model_2',
        'x-android-serial': '789012'
    }

    rv = upload_url(client_loggedin, path, headers_1)
    assert rv.status_code == 200
    url_json1 = rv.get_json()
    print(url_json1)

    rv = upload_url(client_loggedin, path, headers_2)
    assert rv.status_code == 200
    url_json2 = rv.get_json()
    print(url_json2)

    assert url_json1['url'] != url_json2['url']

def test_upload_file(client_loggedin, session):
    path = "/abc/abc/2018-06-21--14-51-35--0/acamera"

    rv = upload_url(client_loggedin, path)
    assert rv.status_code == 200

    url = rv.get_json()['url']
    headers = {'Content-Type': 'application/octet-stream'}
    data = b'my file contents'

    rv = client_loggedin.put(url, headers=headers, data=io.BytesIO(data))
    assert rv.status_code == 200
    assert rv.get_json()['success']

    saved_file_path = '/tmp/' + url.split('/')[-1]
    # Check file exists
    assert os.path.exists(saved_file_path)

    with open(saved_file_path, 'rb') as f:
        file_data = f.read()
    assert file_data == data

