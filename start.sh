#!/bin/ash
set -e

while ! nc -z postgres 5432; do
  sleep 0.1
done

python manage.py db upgrade

if [ "$ENV" = 'DEV' ]; then
  exec python manage.py runserver -h 0.0.0.0
elif [ "$ENV" = 'TEST' ]; then
  exec pytest -v
else
  echo "Running Production Server"
  exec python manage.py runserver -h 0.0.0.0
fi

