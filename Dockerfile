FROM python:3.6-alpine

COPY requirements.txt /

RUN apk update \
    && apk add --virtual build-deps gcc linux-headers musl-dev \
    && apk add postgresql-dev \
    && pip install -r requirements.txt \
    && apk del build-deps

COPY . /app
WORKDIR /app

CMD ["./start.sh"]

