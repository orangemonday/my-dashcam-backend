from flask import g, request, jsonify, url_for

from . import api
from .authentication import auth
from .errors import unauthorized, forbidden, bad_request

from .. import db
from ..models import User, Route

def get_model_serial(ua):
    model = serial = None
    if ua == 'mydashcam-android':
        model = request.headers.get('x-android-model')
        serial = request.headers.get('x-android-serial')
    elif ua == 'mydashcam-ios':
        model = request.headers.get('x-ios-model')
        serial = request.headers.get('x-ios-serial')

    return model, serial

@api.route('/me/')
def userinfo():
    # TODO refactor this into a decorator
    if not g.authenticated:
        return forbidden('Unauthenticated')

    return jsonify(g.user.to_json())

@api.route('/me/upload_url/')
def get_upload_url():
    if not g.authenticated:
        return forbidden('Unauthenticated')

    ua = request.headers.get('User-Agent')
    if ua not in ["mydashcam-android", "mydashcam-ios"]:
        return bad_request('Invalid User-Agent')

    model, serial = get_model_serial(ua)
    if model is None or serial is None:
        return bad_request('Invalid model or serial')

    path = request.args.get('path')
    if path is None:
        return bad_request('Missing path parameter')

    if not Route.verify_path(path):
        return bad_request('Invalid path parameter')

    datetime, part, fn = Route.extract_metadata(path)

    # Get routes
    routes = Route.query.filter_by(
                 name=datetime,
                 author=g.user
             )

    # If routes has at least 1 result, filter again by model and serial
    if routes.count() > 0:
        r = routes.filter_by(model=model, serial=serial).first()
    else:
        r = None

    # If route does not exist, create new one
    if r is None:
        try:
            r = Route(name=datetime,
                      model=model,
                      serial=serial,
                      author=g.user)

            db.session.add(r)
            db.session.commit()
        except:
            return bad_request('Failed to create a new Route')

    url = url_for('api.upload_file', fn=r.get_upload_path(part, fn), _external=True)
    headers = {'Content-Type': 'application/octet-stream'}
    return jsonify({'url': url, 'headers': headers})

# TODO add timeout when user upload stops halfway
# enabling threading help solve the problem of unreponsive Flask
# however not sure about how reliable this method is
@api.route('/me/upload/<fn>', methods=['PUT'])
def upload_file(fn):
    if not g.authenticated:
        return forbidden('Unauthenticated')

    if not request.headers['Content-Type'] == 'application/octet-stream':
        return bad_request('Wrong Content-Type')

    # TODO Change data path
    path = '/tmp/' + fn
    with open(path, 'wb') as f:
        f.write(request.data)
        f.close()

    return jsonify({'success': True})

