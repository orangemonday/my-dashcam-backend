from flask import g, jsonify, current_app, request
from flask_httpauth import HTTPTokenAuth
from google.oauth2 import id_token
from google.auth.transport import requests

from . import api
from .. import db
from ..models import User
from .errors import unauthorized, forbidden, bad_request

auth = HTTPTokenAuth(scheme="JWT")
CLIENT_ID = '800630205148-ttle7g5hiu55bgbn9iegeo5diuj3f91p.apps.googleusercontent.com'

@auth.verify_token
def verify_token(token):
    user = getattr(g, 'user', None)
    if user is None:
        g.user = User.verify_auth_token(token)
        g.authenticated = g.user is not None
    return True

@api.route('/auth/signin/', methods=['POST'])
def get_token():
    id_info = google_oauth()
    if id_info is None:
        return unauthorized('Invalid credentials')

    user = User.query.filter_by(google_id=id_info['sub']).first()
    if user is None:
        return jsonify({'registered': False})

    return jsonify({'token': user.generate_auth_token(3600),
        'expiration': 3600,
        'registered': True,
        'email': user.email,
        'name': user.name
    })

@api.route('/auth/register/', methods=['POST'])
def register():
    id_info = google_oauth()
    if id_info is None:
        return unauthorized('Invalid credentials')

    name = request.json.get('name')
    if name is None:
        return bad_request('Missing name parameter')

    user = User.query.filter_by(google_id=id_info['sub']).first()
    if user is not None:
        return bad_request('User is registered')
    
    user = User.query.filter_by(name=name).first()
    if user is not None:
        return bad_request('Username is used')

    try:
        user = User(name=name,
                    email=id_info['email'],
                    google_id=id_info['sub'])

        db.session.add(user)
        db.session.commit()
    except:
        return bad_request('Username is invalid')

    return jsonify({'token': user.generate_auth_token(3600),
        'expiration': 3600,
        'email': user.email,
        'name': user.name
    })

@api.before_request
@auth.login_required
def before_request():
    pass

def google_oauth():
    try:
        idtoken = request.json.get('id_token')
        if idtoken is None:
            return None

        id_info = id_token.verify_oauth2_token(idtoken, requests.Request(), CLIENT_ID)

        if id_info['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
            raise ValueError('Wrong issuer.')
        
        return id_info
    except:
        return None

@auth.error_handler
def auth_error():
    return unauthorized('Invalid credentials')

