from flask import render_template
from . import main

@main.route('/', methods=['GET'])
def index():
    return render_template('index.html')

@main.route('/signin/', methods=['GET'])
def login():
    return render_template('signin.html')

