import re
import uuid

from hashlib import md5
from datetime import datetime
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from sqlalchemy.orm import validates

from flask import current_app
from . import db

class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.String(16), default=lambda: uuid.uuid1().hex[:16], primary_key=True)

    google_id = db.Column(db.Numeric, unique=True, index=True, nullable=False)
    email = db.Column(db.String(64), unique=True, index=True, nullable=False)
    name = db.Column(db.String(64), unique=True, nullable=False)

    point = db.Column(db.Integer, default=0)
    member_since = db.Column(db.DateTime(), default=datetime.utcnow)

    routes = db.relationship('Route', backref='author', lazy='dynamic')

    @validates('name')
    def validate_name(self, key, name):
        assert User.query.filter(User.name == name).first() is None

        assert len(name) > 2

        return name

    @validates('email')
    def validate_email(self, key, email):
        assert re.match("[^@]+@[^@]+\.[^@]+", email)

        return email

    def generate_auth_token(self, expiration):
        s = Serializer(current_app.config['SECRET_KEY'])
        # expires_in=expiration)
        return s.dumps({'id': self.id}).decode('utf-8')

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return None
        return User.query.get(data['id'])

    def to_json(self):
      json_user = {
        'name': self.name,
        'email': self.email
      }

      return json_user
    
    def __repr__(self):
        return '<User %r>' % self.name

class Route(db.Model):
    __tablename__ = 'routes'
    id = db.Column(db.String, default=lambda: uuid.uuid1().hex, primary_key=True)

    name = db.Column(db.String(32), nullable=False)
    model = db.Column(db.String(32), nullable=False)
    serial = db.Column(db.String(32), nullable=False)
    author_id = db.Column(db.String(16), db.ForeignKey('users.id'), nullable=False)

    @staticmethod
    def verify_path(path):
        if re.match(".+\/[0-9\-]+\-\-[0-9\-]+\-\-[0-9]+\/(log\.gz|acamera)$", path):
            return True

        return False

    @staticmethod
    def extract_metadata(path):
        # */time/filetype
        path = path.split('/')

        # filetype: [log.gz | acamera]
        fn = path[-1]

        # time: date--time--part
        name = path[-2].split('--')
        datetime = '--'.join(name[:-1])
        part = name[-1]

        return datetime, part, fn

    def get_upload_path(self, part, fn):
        return self.id + '_' + self.name + '--' + part + '-' + fn

    def get_sig_path(self):
        return self.author_id + '/' + self.id + '_' + self.name

    def __repr__(self):
        return '<Route %r>' % (self.author_id + '|' + self.name)

