import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'hard to guess string'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    @staticmethod
    def init_app(app):
        pass

class DevelopmentConfig(Config):
    DEBUG = True
    ENV = 'DEV'

class TestingConfig(Config):
    DEBUG = True
    ENV = 'TEST'

class ProductionConfig(Config):
    ENV = 'PROD'

config = {
 'DEV': DevelopmentConfig,
 'TEST': TestingConfig,
 'PROD': ProductionConfig,
 'default': DevelopmentConfig
}

